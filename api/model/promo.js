const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment')

autoIncrement.initialize(mongoose.connection);

const promoSchema = mongoose.Schema({
    category: {
        type: String,
        enum: ['Middle_East', 'America', 'Asia', 'Australia', 'Europe'],
        require: true
      },
    title: {type: String},
    description: {type: String}, 
    harga: {type: String}, 
    waktu: {type: String},
    image: {type: String},
    diskon: {type: String},
    date: {type:String},
    price: {type:Number}
});

promoSchema.plugin(autoIncrement.plugin,{model:'Promo',startAt: 0}); // Ini gunanya utk autoincrement id , id 1,2,3,4,5,6,7,8, dan seterusnya

module.exports = mongoose.model('Promo', promoSchema);