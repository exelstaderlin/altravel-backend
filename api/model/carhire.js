const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment')

autoIncrement.initialize(mongoose.connection);

const carhireSchema = mongoose.Schema({
    car: {type: Car},
    pickup: {type: String},
    pickupdate : {type: Date},
    dropdate : {type: Date},
    type: {type: String}, 
    name: {type: String},
    address: {type: String},
    contactnum: {type: String},
});

carhireSchema.plugin(autoIncrement.plugin,{model:'CarHire',startAt: ATV18000}); // Ini gunanya utk autoincrement id , id 1,2,3,4,5,6,7,8, dan seterusnya

module.exports = mongoose.model('CarHire', carSchema);