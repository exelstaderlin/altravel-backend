const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment')

autoIncrement.initialize(mongoose.connection);

const carSchema = mongoose.Schema({
    cartype: {
        type: String,
        enum: ['Avanza (7 seats)', 'Hyundai H1 (10 seats)', 'Serena HWS (5 seats)'],
        require: true
      },
    destination: {type: String},
    type: {type: String}, 
    price: {type: String},
    image: {type: String}
});

carSchema.plugin(autoIncrement.plugin,{model:'Car',startAt: 0}); // Ini gunanya utk autoincrement id , id 1,2,3,4,5,6,7,8, dan seterusnya

module.exports = mongoose.model('Car', carSchema);