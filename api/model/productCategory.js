const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment')

autoIncrement.initialize(mongoose.connection);

const productSchema = mongoose.Schema({
    title: {type: String},
    category: {type: String}, 
    description: {type: String},
    image: {type: String}
});

productSchema.plugin(autoIncrement.plugin,{model:'ProductCategory',startAt: 0}); // Ini gunanya utk autoincrement id , id 1,2,3,4,5,6,7,8, dan seterusnya

module.exports = mongoose.model('ProductCategory', productSchema);