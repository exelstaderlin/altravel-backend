const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment')

autoIncrement.initialize(mongoose.connection);

const userSchema = mongoose.Schema({
    email: { 
        type: String, 
        required: true, 
        unique: true, 
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    name : {type:String},
    contact : {type : String},
    city: {type: String},
    password: { type: String, required: true }
});


userSchema.plugin(autoIncrement.plugin, 'User'); // Ini gunanya utk autoincrement id , id 1,2,3,4,5,6,7,8, dan seterusnya

module.exports = mongoose.model('User', userSchema);