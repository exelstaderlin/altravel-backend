const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment')

autoIncrement.initialize(mongoose.connection);

const bookingSchema = mongoose.Schema({
    name: {type: String},
    email: {type: String},
    contact: {type: String}, 
    title: {type: String}, 
    days: {type: String},
    date: {type: String},
    date_time: {type: String},
    image: {type:String},
    adult: {type: String},
    child: {type: String},
    total_price: {type: String},
    code: {type: String},

});

bookingSchema.plugin(autoIncrement.plugin,{model:'Booking',startAt: 0}); // Ini gunanya utk autoincrement id , id 1,2,3,4,5,6,7,8, dan seterusnya

module.exports = mongoose.model('Booking', bookingSchema);