const express = require("express"); 
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Booking = require("../model/booking");
const db = require("../../app");


router.get('/booking/:email',(req,res) =>{ //get all booking
    Booking.find({email: req.params.email})
    .select("id name email contact title days date date_time image adult child total_price code")
    .exec()
    .then(docs => {
      const response = {
        status: true,
        data: docs.map(doc => {
          return {
            id: doc.id,
            name: doc.name,
            email: doc.email,
            contact: doc.contact, 
            title: doc.title, 
            days: doc.days,
            date: doc.date,
            date_time: doc.date_time,
            image: doc.image,
            adult: doc.adult,
            child: doc.child,
            total_price: doc.total_price,
            code: doc.code,
          };
        }),
        total: docs.length,
      };
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
  });


router.post('/booking',function(req,res){  // TAMBAH PRODUCT
    const booking = new Booking({
        id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        email: req.body.email,
        contact: req.body.contact, 
        title: req.body.title, 
        days: req.body.days,
        date: req.body.date,
        date_time: req.body.date_time,
        image: req.body.image,
        adult: req.body.adult,
        child: req.body.child,
        total_price: req.body.total_price,
        code: req.body.code,
      });
      booking
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            status : true,
            message: "Booking success"
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });

});



module.exports = router;