const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const User = require("../model/user");

const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';


router.post("/check_email", (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(200).json({
          status : true,
          message: req.body.email
        });
      }
      else{
        return res.status(200).json({
          status : false,
          message: "Email doesnt exist"
        });
      } 
    });
});


router.post("/signup", (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          status : false,
          message: "Mail exists"
        });
      } else {
        bcrypt.genSalt(saltRounds, function(err, salt) {
          bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              return res.status(500).json({
              });
                error: err.toString()
            } else {
              const user = new User({
                email: req.body.email,
                name: req.body.name,
                contact: req.body.contact,
                city: req.body.city,
                password: hash
              });
              user
                .save()
                .then(result => {
                  console.log(result);
                  res.status(201).json({
                    status : true,
                    message: "User created"
                  });
                })
                .catch(err => {
                  console.log(err);
                  res.status(500).json({
                    error: err
                  });
                });
            }
          });
        });
      }
    });
});


router.post("/login", (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          status : false,
          message: "User not found, Email doesn't exist"
        });
      }
      else{
        bcrypt.compare(req.body.password, user[0].password, function(err,result) {
          console.log(req.body.password)
          console.log(user[0].password)
          console.log(err)
          console.log(result)

          if (err) {
            return res.status(401).json({
              status : false,
              error: err.toString()
            });
          }
          if (result) {
            return res.status(200).json({
              status : true,
              data: {
                  id: user[0].id,
                  name: user[0].name,
                  email:user[0].email,
                  contact: user[0].contact,
                  city: user[0].city,
              },
              message: "Login successful"
            });
          }
          else{
            return res.status(200).json({
              status : false,
              message: "Password wrong"
            });
          }
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});


router.delete("/delete/:userId", (req, res, next) => {
  User.remove({ _id: req.params.userId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "User deleted"
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;
