const express = require("express");
const router = express.Router(); 
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Car = require("../model/car");
const db = require("../../app");

var cartype = [
  {id : 1, cartype : "Avanza (7 seats)",image:"http://www.toyota-bandung.co.id/wp-content/uploads/2017/11/Toyota-Grand-New-Avanza-Silver-Metallic.png"},
  {id : 2, cartype : "Hyundai H1 (10 seats)",image: "http://www.eurospeed.co.za/wp-content/uploads/2015/12/Starex.png"},
  {id : 3, cartype : "Serena HWS (5 seats)",image:"https://s4.paultan.org/cn/image/2018/04/10-ALL-NEW-SERENA_PHS_IMPERIAL-UMBER-CASHMERE-GOLD-e1523587838695.jpg"}]


var photos = [ {
  albumId: 1,
  id: 1,
  title: "accusamus beatae ad facilis cum similique qui sunt",
  url: "https://via.placeholder.com/600/92c952",
  thumbnailUrl: "https://via.placeholder.com/150/92c952"
},
{
  albumId: 1,
  id: 2,
  title: "reprehenderit est deserunt velit ipsam",
  url: "https://via.placeholder.com/600/771796",
  thumbnailUrl: "https://via.placeholder.com/150/771796"
},
{
  albumId: 1,
  id: 2,
  title: "reprehenderit est deserunt velit ipsam",
  url: "https://via.placeholder.com/600/771796",
  thumbnailUrl: "https://via.placeholder.com/150/771796"
},
{
  albumId: 1,
  id: 2,
  title: "reprehenderit est deserunt velit ipsam",
  url: "https://via.placeholder.com/600/771796",
  thumbnailUrl: "https://via.placeholder.com/150/771796"
}
]

router.get('/car',(req,res) =>{ //get all car
  Car.find()
  .select("id cartype destination type price image")
  .exec() 
  .then(docs => {

    const response = {
      status: true,
      data: docs.map(doc => {
        return {
          id: doc.id,
          cartype: doc.cartype,
          destination: doc.destination,
          type: doc.type,
          price: doc.price,
          image: doc.image,
        };
      }),
      total: docs.length,
    };
    res.status(200).json(response);
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});



router.get('/test',function(req,res){ //testing
  var topping = [{id:"5001", type:"None"},{id:"5002", type:"Glazed"},{id:"5003", type:"Sugar"},{id:"5004", type:"Powdered Sugar"},{id:"5006", type:"Chocolate"},]
  var batter = [{id:"1001", type:"Regular"},{id:"1002", type:"Chocolate"},{id:"1003", type:"Blueberry"},{id:"1004", type:"Devil's Food"},]
  var data = [{id:"0001", type:"donut",name:"Cake",batters:{batter: batter},topping : topping}]
  res.status(200).json({
      items : data
  })
});


router.get('/car/cartype',function(req,res){
      res.status(200).json({
          status : true,
          data : cartype
      })
});

router.get('/photos',function(req,res){
  res.status(200).json({
      status : true,
      data : photos
  })
});

router.get('/car/cartype/:cartype',function(req,res){
    Car.find({ cartype: req.params.cartype })
    .select("id cartype destination type price image")
    .exec()
    .then(docs => {
      const response = {
        status: true,
        count: docs.length,
        data: docs.map(doc => {
          return {
            id: doc.id,
            cartype: doc.cartype,
            destination: doc.destination,
            type: doc.type,
            price: doc.price,
            image: doc.image
          };
        })
      };
      res.status(200).json(response);
     })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.post('/car',function(req,res){
    const car = new Car({
        id: new mongoose.Types.ObjectId(),
        destination: req.body.destination,
        cartype: req.body.cartype,
        price: req.body.price,
        type: req.body.type,
        image: req.body.image
      });
      car
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            status : true,
            message: "Car created"
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
});

module.exports = router;