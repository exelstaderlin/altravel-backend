const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Promo = require("../model/promo");
const db = require("../../app");

var category = [{id : 1, category : "Asia"},{id : 2, category : "Europe"},{id : 3, category : "Middle East"},{id : 4, category : "America"},{id : 5, category : "Australia"}]

router.get('/promo',(req,res) =>{ //get all promo
  Promo.find()
  .select("id category title description waktu diskon harga date image price")
  .exec()
  .then(docs => {
    const response = {
      status: true,
      data: docs.map(doc => {
        return {
          id: doc.id,
          category: doc.category,
          title: doc.title,
          description: doc.description,
          waktu:doc.waktu,
          harga:doc.harga,
          diskon:doc.diskon,
          date:doc.date,
          image:doc.image,
          price:doc.price
          // request: {
          //   type: "GET",
          //   url: "http://localhost:3000/promos/" + doc._id
          // }
        };
      }),
      total: docs.length,
    };

    res.status(200).json(response);
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.get('/promo/category',function(req,res){
      res.status(200).json({
          status : true,
          data : category
      })
});

router.get('/promo/category/:category',function(req,res){
    Promo.find({ category: req.params.category })
    .select("id category title description harga waktu image diskon date price")
    .exec()
    .then(docs => {
      const response = {
        status: true,
        count: docs.length,
        data: docs.map(doc => {
          return {
            id: doc.id,
            category: doc.category,
            title: doc.title,
            description: doc.description,
            harga: doc.harga,
            waktu:doc.waktu,
            image: doc.image,
            diskon: doc.diskon,
            date: doc.date,
            price: doc.price
          };
        })
      };
      res.status(200).json(response);
     })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.post('/promo',function(req,res){
    const promo = new Promo({
        id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        category: req.body.category,
        description: req.body.description,
        harga: req.body.harga,
        waktu: req.body.waktu,
        image: req.body.image,
        diskon: req.body.diskon,
        date: req.body.date,
        price: req.body.price
      });
      promo
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            status : true,
            message: "Promo created"
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
});

module.exports = router;