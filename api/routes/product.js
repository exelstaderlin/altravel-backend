const express = require("express"); 
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Product = require("../model/product");
const ProductCategory = require("../model/productCategory");

const db = require("../../app");

//const productData  = require('../model/product')
// var collection = db.product.find();
router.get('/product',(req,res) =>{ //get all product
  Product.find()
  .select("id category title description waktu harga date image price")
  .exec()
  .then(docs => {
    const response = {
      status: true,
      data: docs.map(doc => {
        return {
          id: doc.id,
          category: doc.category,
          title: doc.title,
          description: doc.description,
          waktu:doc.waktu,
          harga:doc.harga,
          date:doc.date,
          image:doc.image,
          price: doc.price,
          request: {
            type: "GET",
            url: "http://localhost:3000/products/" + doc._id
          }
        };
      }),
      total: docs.length,
    };
    res.status(200).json(response);
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

// router.get('/product/category',function(req,res){
//       res.status(200).json({
//           status : true,
//           data : category
//       })
// });

router.get('/product/category',function(req,res){
  ProductCategory.find()
    .select("id title category description image")
    .exec()
    .then(docs => {
      const response = {
        status: true,
        data: docs.map(doc => {
          return {
            id: doc.id,
            title: doc.title,
            category: doc.category,
            description: doc.description,
            image: doc.image
          };
        }),
        total: docs.length,
      };
      res.status(200).json(response);
     })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.get('/product/category/:category',function(req,res){ // TAMBAH PRODICT BERDASAR CATEGORY
    Product.find({ category: req.params.category })
    .select("id category title description harga waktu image date price")
    .exec()
    .then(docs => {
      const response = {
        status: true,
        data: docs.map(doc => {
          return {
            id: doc.id,
            category: doc.category,
            title: doc.title,
            description: doc.description,
            harga: doc.harga,
            waktu:doc.waktu,
            image: doc.image,
            date: doc.date,
            price: doc.price
          };
        }),
        total: docs.length,
      };
      res.status(200).json(response);
     })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  });
});

router.post('/product',function(req,res){  // TAMBAH PRODUCT
    const product = new Product({
        id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        category: req.body.category,
        description: req.body.description,
        harga: req.body.harga,
        waktu: req.body.waktu,
        image: req.body.image,
        date: req.body.date,
        price: req.body.price
      });
      product
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            status : true,
            message: "Product created"
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
});


router.post('/product/category',function(req,res){  // TAMBAH PRODUCT CATEGORY SEPERTI ASIA AMERICA DLL
  const productCategory = new ProductCategory({
      id: new mongoose.Types.ObjectId(),
      title: req.body.title,
      category: req.body.category,
      description: req.body.description,
      image: req.body.image,
    });
    productCategory
      .save()
      .then(result => {
        console.log(result);
        res.status(201).json({
          status : true,
          message: "ProductCategory created"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });

});

module.exports = router;