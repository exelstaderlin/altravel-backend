const express = require('express')
const app = express()
const morgan = require('morgan')
const product = require('./api/routes/product')
const user = require('./api/routes/user')
const promo = require('./api/routes/promo')
const car = require('./api/routes/car')
const booking = require('./api/routes/booking')

const mongoose = require('mongoose');
const bodyParser = require("body-parser");

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api',product)
app.use('/api',user)
app.use('/api',promo)
app.use('/api',car)
app.use('/api',booking)

// JGN LUPA CONNECT KE DATABASE MELALUI MONGO SHELL, >mongo ds239930.mlab.com:39930/altravel-db -u user -p admin1

// var mongoDB = 'mongodb://localhost/test';
var mongoDB = 'mongodb://user:admin1@ds239930.mlab.com:39930/altravel-db';

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error'));
db.once('open', function () {
  console.log('connected to mongoDb')
});

app.use(function(req,res,next){
    const error = Error('Page Not Found')
    error.status = 404
    next(error)
})


module.exports = app